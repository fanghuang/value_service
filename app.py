'''
Running:

  PYTHONPATH=. python examples/basic.py

'''

import logging
from flask import Flask, redirect
from flask.ext.restful import reqparse, abort, Api, Resource, fields,\
    marshal_with
from flask_restful_swagger import swagger
from redis import Redis

app = Flask(__name__, static_folder='./static')
redis = Redis(host='127.0.0.1', port=6379, db=0)
logging.basicConfig(filename='event.log', level=logging.DEBUG)

###################################
# This is important:
api = swagger.docs(Api(app), apiVersion='0.1',
                   basePath='http://localhost:5000',
                   resourcePath='/',
                   produces=["application/json", "text/html"],
                   api_spec_url='/api/spec',
                   description='A Basic API')
###################################

tenants = {
    'df': 'a',
    'm1': 'b',
    'ccc': 'c',
    'm2': 'd'
}

def abort_if_it_doesnt_exist(id):
  if id not in tenants:
    abort(404, message="Todo {} doesn't exist".format(id))

parser = reqparse.RequestParser()
parser.add_argument('task', type=str)


@swagger.model
class TodoItem:
  """This is an example of a model class that has parameters in its constructor
  and the fields in the swagger spec are derived from the parameters
  to __init__.
  In this case we would have args, arg2 as required parameters and arg3 as
  optional parameter."""
  def __init__(self, arg1, arg2, arg3='123'):
    pass

class Iid(Resource):
  "Iid API"
  @swagger.operation(
      # notes='',
      nickname='get',
      # Parameters can be automatically extracted from URLs (e.g. <string:id>)
      # but you could also override them here, or add other parameters.
      parameters=[
          {
            "name": "tenant_name",
            "description": "The name or ID of the tenant",
            "required": True,
            "allowMultiple": False,
            "dataType": 'string',
            "paramType": "path"
          },
          {
            "name": "model_name",
            "description": "The name or ID of the model",
            "required": True,
            "allowMultiple": False,
            "dataType": 'string',
            "paramType": "path"
          }
      ])
  def get(self, tenant_name, model_name):
    # This goes into the summary
    """Get an incrementing ID

    Returns an unique imcrementing ID for each model of the tenant.
    """
    redis.hincrby(tenant_name, model_name)
    logging.debug({str(tenant_name) + '/' + str(model_name): \
      long(redis.hget(tenant_name, model_name))})
    return {"tenant_name": tenant_name, \
            "model_name": model_name, \
            "iid": long(redis.hget(tenant_name, model_name))}, \
            200, {'Access-Control-Allow-Origin': '*'}

  def options (self, **args):
    # since this method is not decorated with @swagger.operation it does not
    # get added to the swagger docs
    return {'Allow' : 'GET,PUT,POST,DELETE' }, 200, \
    { 'Access-Control-Allow-Origin': '*', \
      'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE', \
      'Access-Control-Allow-Headers': 'Content-Type' }

@swagger.model
class ModelWithResourceFields:
  resource_fields = {
      'a_string': fields.String()
  }

@swagger.model
@swagger.nested(
   a_nested_attribute=ModelWithResourceFields.__name__,
   a_list_of_nested_types=ModelWithResourceFields.__name__)
class TodoItemWithResourceFields:
  """This is an example of how Output Fields work
  (http://flask-restful.readthedocs.org/en/latest/fields.html).
  Output Fields lets you add resource_fields to your model in which you specify
  the output of the model when it gets sent as an HTTP response.
  flask-restful-swagger takes advantage of this to specify the fields in
  the model"""
  resource_fields = {
      'a_string': fields.String(attribute='a_string_field_name'),
      'a_formatted_string': fields.FormattedString,
      'an_enum': fields.String,
      'an_int': fields.Integer,
      'a_bool': fields.Boolean,
      'a_url': fields.Url,
      'a_float': fields.Float,
      'an_float_with_arbitrary_precision': fields.Arbitrary,
      'a_fixed_point_decimal': fields.Fixed,
      'a_datetime': fields.DateTime,
      'a_list_of_strings': fields.List(fields.String),
      'a_nested_attribute': fields.Nested(ModelWithResourceFields.resource_fields),
      'a_list_of_nested_types': fields.List(fields.Nested(ModelWithResourceFields.resource_fields)),
  }

  # Specify which of the resource fields are required
  required = ['a_string']

  swagger_metadata = {
      'an_enum': {
          'enum': ['one', 'two', 'three']
      }
  }

##
## Actually setup the Api resource routing here
##
api.add_resource(Iid, '/get_iid/<string:tenant_name>/<string:model_name>')


@app.route('/docs')
def docs():
  return redirect('/static/docs.html')


if __name__ == '__main__':
  TodoItemWithResourceFields()
  TodoItem(1, 2, '3')
  app.run(debug=True, host='0.0.0.0', port=8080)
